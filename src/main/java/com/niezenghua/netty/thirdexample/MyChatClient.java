package com.niezenghua.netty.thirdexample;

import com.niezenghua.protobuf.DataInfo;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;

import javax.xml.crypto.Data;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Scanner;

public class MyChatClient {

    public static void main(String[] args) throws Exception {
        EventLoopGroup eventLoopGroup = new NioEventLoopGroup();

        try{
            Bootstrap bootstrap = new Bootstrap();
            bootstrap.group(eventLoopGroup).channel(NioSocketChannel.class).
                    handler(new MyChatClientInitializer());

            Channel channel = bootstrap.connect("127.0.0.1",8899).sync().channel();

//            Scanner sc = new Scanner(System.in);
//            System.out.println("请输入姓名：");
//            String name = sc.nextLine();
//            System.out.println("请输入年龄");
//            int age = sc.nextInt();
//            System.out.println("请输入地址");
//            String address = sc.nextLine();
//
//            DataInfo.Student student = DataInfo.Student.newBuilder().setName(name).setAge(age).setAddress(address).build();
//
//            channel.writeAndFlush(student.toByteArray());

            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

            for(;;){
                channel.writeAndFlush(br.readLine() + "\r\n");
            }

        } finally {
            eventLoopGroup.shutdownGracefully();
        }
    }
}
