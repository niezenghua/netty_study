package com.niezenghua.netty.sixthexample;

import com.niezenghua.protobuf.DataInfo;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

import java.util.Random;

public class TestClientHandler extends SimpleChannelInboundHandler<DataInfo.MyMessage> {

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, DataInfo.MyMessage msg) throws Exception {

    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        int randomInt = new Random().nextInt(3);
        DataInfo.MyMessage myMessage = null;

        if (0 == randomInt) {
            myMessage = DataInfo.MyMessage.newBuilder().
                    setDataType(DataInfo.MyMessage.DataType.StudentType).
                    setStudent(DataInfo.Student.newBuilder().
                            setName("张三").setAge(20).
                            setAddress("南京").build()).build();
        } else if (1 == randomInt) {
            myMessage = DataInfo.MyMessage.newBuilder().
                    setDataType(DataInfo.MyMessage.DataType.DogType).
                    setDog(DataInfo.Dog.newBuilder().
                            setName("小汪").setAge(2).build()).build();
        } else {
            myMessage = DataInfo.MyMessage.newBuilder().
                    setDataType(DataInfo.MyMessage.DataType.CatType).
                    setCat(DataInfo.Cat.newBuilder().
                            setName("小花").setCity("南京").build()).build();
        }

        ctx.channel().writeAndFlush(myMessage);
    }
}
