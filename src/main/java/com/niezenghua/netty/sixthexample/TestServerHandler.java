package com.niezenghua.netty.sixthexample;

import com.niezenghua.protobuf.DataInfo;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

public class TestServerHandler extends SimpleChannelInboundHandler<DataInfo.MyMessage> {

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, DataInfo.MyMessage msg) throws Exception {
        DataInfo.MyMessage.DataType dataType = msg.getDataType();

        if (DataInfo.MyMessage.DataType.StudentType == dataType){
            System.out.println("Message info is Student");
            System.out.println(msg.getStudent().getName());
            System.out.println(msg.getStudent().getAge());
            System.out.println(msg.getStudent().getAddress());
        }

        if (DataInfo.MyMessage.DataType.DogType == dataType) {
            System.out.println("Message info is Dog");
            System.out.println(msg.getDog().getName());
            System.out.println(msg.getDog().getAge());
        }

        if (DataInfo.MyMessage.DataType.CatType == dataType) {
            System.out.println("Message info is Cat");
            System.out.println(msg.getCat().getName());
            System.out.println(msg.getCat().getCity());
        }
    }
}
