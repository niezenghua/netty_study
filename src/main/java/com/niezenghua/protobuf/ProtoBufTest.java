package com.niezenghua.protobuf;

public class ProtoBufTest {

    public static void main(String[] args) throws Exception {

        DataInfo.Student student = DataInfo.Student.newBuilder().
                setName("张三").setAge(18).setAddress("南京").build();

        byte[] student2ByteArrary = student.toByteArray();

        DataInfo.Student student1 = DataInfo.Student.parseFrom(student2ByteArrary);

        System.out.println(student1.getName());
        System.out.println(student1.getAge());
        System.out.println(student1.getAddress());

    }
}
